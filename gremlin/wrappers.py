import logging
import pathlib

from .util import prompt_yn

def _rm_interactive(gremlin,
        delete_on_copy,
        disable_blacklist,
        path):
    if prompt_yn("remove {}".format(path)):
        gremlin.rm(path, delete_on_copy, disable_blacklist)

def _rm(gremlin,
        is_interactive,
        delete_on_copy,
        disable_blacklist,
        path):
    if is_interactive:
        _rm_interactive(gremlin, delete_on_copy, disable_blacklist, path)
    else:
        gremlin.rm(path, delete_on_copy, disable_blacklist)

def rm_all(gremlin,
        is_interactive,
        delete_on_copy,
        disable_blacklist,
        *paths):
    """Performs a safe-delete operation on a list of paths.

    This is a convenience wrapper operating on a list.

    Args:
        gremlin (:obj:`Gremlin`): an instance of gremlin class

        is_interactive (bool): If True, performs interactive removal on provided paths.

        delete_on_copy (bool): If True, files will be deleted if they exist on a different filesystem than the storage path.

        disable_blacklist (bool): If True, paths won't be checked with blacklist.

        paths (pathlib.Path): An \*args of paths.
    """
    for path in paths:
        try:
            _rm(gremlin, is_interactive, delete_on_copy, disable_blacklist,
                    pathlib.Path(path))
        except Exception as e:
            logging.error("failed to safely delete [%s] -> %s" % (path, str(e)))

def _restore_interactive(gremlin, dst_path, restore_paths, entry_uuid):
    if prompt_yn("restore {}".format(entry_uuid)):
        gremlin.restore(entry_uuid, restore_paths, dst_path)

def _restore(gremlin,
        dst_path,
        restore_paths,
        is_interactive,
        entry_uuid):
    if is_interactive:
        _restore_interactive(gremlin, dst_path, restore_paths, entry_uuid)
    else:
        gremlin.restore(entry_uuid, restore_paths, dst_path)

def restore_all(gremlin,
        dst_path,
        restore_paths,
        is_interactive,
        *entry_uuids):
    """Restores paths from provided list.

    Args:
        gremlin (:obj:`Gremlin`): an instance of gremlin class

        restore_paths(bool): If True, an attempt to restore parent paths will be made.

        is_interactive (bool): If True, performs interactive removal on provided paths.

        entry_uuids: An \*args of UUIDs to be restored.
    """
    if len(dst_path):
        dst_path = pathlib.Path(dst_path)
    else:
        dst_path = None

    for entry_uuid in entry_uuids:
        _restore(gremlin,
                dst_path,
                restore_paths,
                is_interactive,
                entry_uuid)

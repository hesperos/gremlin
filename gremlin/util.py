import logging
import sys
import uuid

def prompt_yn(question):
    """Simple wrapper performing an interactive yes/no query.

    It will attempt to get an answer to the provided yes/no question,
    retrying on invalid input.

    Args:
        question (str): A question that will be printed to the user.

    Returns:
        bool: True, if provided answer is 'yes'. False otherwise.
    """
    while True:
        reply = str(raw_input(question+'? (y/n): ')).lower().strip()
        return True if reply[0] == 'y' else False

def is_interpreter_supported():
    """Checks if the interpreter installed with the system is supported.

    Returns:
        bool: True if supported, False otherwise.
    """
    return sys.version_info.major >= 3

def are_paths_on_same_device(p1, p2):
    """Checks if the provided path exist on the same filesystem
    Args:
        p1 (:obj:`pathlib.Path`): first path.
        p2 (:obj:`pathlib.Path`): second path.

    Returns:
        bool: True, if both paths exist on the same filesystem. False otherwise.
    """
    s1 = p1.stat()
    s2 = p2.stat()
    return s1.st_dev == s2.st_dev if s1 and s2 else False

def is_uuid_valid(entry_uuid):
    """Checks if the provided UUID is valid

    Args:
        entry_uuid (str/uuid.UUID): UUID to be verified.

    Returns:
        uuid.UUID: an instance of UUID object if UUID is valid. None, otherwise.
    """
    if isinstance(entry_uuid, uuid.UUID):
        return entry_uuid

    try:
        return uuid.UUID(entry_uuid)
    except Exception as e:
        logging.error("invalid uuid: %s", str(e))

    return None

import datetime
import jsonschema
import logging
import shutil
import time
import uuid
import yaml

from pathlib import Path

from . import gremlin_config
from . import util

class Gremlin(object):
    """Gremlin implements a safe-deleter API. The object manages a
    file system based database containing the metadata for the deleted
    items along with the data itself.

    On construction, storage directory creation will be attempted (if it
    doesn't exist already). This may raise exceptions if the filesystem
    locations provided by configuration are inaccessible or the directory
    creation files due to other reasons.

    Args:
        config (GremlinConfig): interface compatible object providing basic configuration and settings.

    Returns:
        Gremlin: New instance of Gremlin object.

    Raises:
        PermissionDenied: if storage directory creation fails.
    """

    metadata_schema = yaml.load("""type: object
properties:
    original_path:
        type: string
    orig_atime:
        type: number
    orig_mtime:
        type: number
    orig_ctime:
        type: number
    uid:
        type: integer
    gid:
        type: integer
    mode:
        type: integer
    size:
        type: integer
    when_deleted:
        type: number
    is_directory:
        type: boolean""", Loader = yaml.FullLoader)

    def __init__(self, config):
        self.config = config
        self._create_storage()

    def _get_storage_bucket_path(self, bucket_uuid):
        return self.config.get_storage_path().joinpath(bucket_uuid)

    def _create_storage_bucket(self):
        bucket_id = str(uuid.uuid4())
        bucket_path = self._get_storage_bucket_path(bucket_id)
        data_path = bucket_path.joinpath(self.config.get_data_dirname())

        try:
            bucket_path.mkdir(
                    mode = self.config.get_directory_mode(),
                    parents = True)
            data_path.mkdir(
                    mode = self.config.get_directory_mode(),
                    parents = True)
            logging.debug("created bucket: %s" % (bucket_path))

        except OSError as e:
            logging.error("unable to create storage bucket: %s" % (str(e)))
            raise

        return bucket_path, data_path

    def _create_storage(self):
        storage_path = self.config.get_storage_path()
        if not storage_path.exists():
            storage_path.mkdir(
                    mode = self.config.get_directory_mode())

    def _create_metadata(self, metadata_path, orig_path):
        sr = orig_path.stat()
        is_directory = orig_path.is_dir()
        when_deleted = time.time()
        data = {
                'original_path': str(orig_path.absolute()),
                'orig_atime': sr.st_atime,
                'orig_mtime': sr.st_mtime,
                'orig_ctime': sr.st_ctime,
                'uid': sr.st_uid,
                'gid': sr.st_gid,
                'mode': sr.st_mode,
                'size': sr.st_size,
                'when_deleted': when_deleted,
                'is_directory': is_directory,
                }
        metadata_path.write_text(yaml.dump(data))

    def _load_metadata(self, bucket_path):
        metadata_path = bucket_path.joinpath(
                self.config.get_metadata_filename())

        try:
            if not metadata_path.exists():
                logging.error("metadata missing: %s" % (metadata_path))
                return None

            metadata = yaml.safe_load(metadata_path.read_text())
            jsonschema.validate(metadata, Gremlin.metadata_schema)
            return metadata

        except jsonschema.ValidationError as e:
            logging.error("metadata malformed: %s" % (str(e)))

        except ImportError as e:
            logging.error("unable to load metadata: %s" % (str(e)))

        except Exception as e:
            logging.error("failed to collect metadata: %s" % (str(e)))

        return None

    def _force_remove(self, path):
        if path.is_dir():
            shutil.rmtree(path)
        else:
            path.unlink()

    def rm(self, path, delete_on_copy, disable_blacklist):
        """
        Performs safe-deletion of given path.

        Moves the file to internal storage location. If the file spans
        different devices it will be either copied or deleted 'normally'
        without copying (depending if delete_on_copy is True)

        Args:
            path (pathlib.Path): The path to be safely-deleted.

            delete_on_copy (bool): If True and file is on different devices
                than the storage path, the file will be permanently deleted with
                fallback created. If False, the file will be copied. Has no effect
                if both the path to be removed and the storage path exist on the
                same filesystem.

            disable_blacklist (bool): If True, the given path won't be
                confronted with the blacklist. If False, the path will be
                checked against the blacklist, if there's a match, the
                request will be denied with an exception.

        Raises:
            Exception: in case of any filesystem error.
            RuntimeError: in case the given path is blacklisted.
        """
        if not disable_blacklist:
            bl_paths, bl_files = self.config.get_blacklist()
            p_str = str(path)
            if p_str in bl_paths or p_str in bl_files:
                logging.error("path is blacklisted!")
                raise RuntimeError("attempt to remove blacklisted path/file")

        if not util.are_paths_on_same_device(path, self.config.get_storage_path()):
            logging.info("paths are on different devices")
            if delete_on_copy:
                logging.info("traditionally deleting path [{}] as requested")
                self._force_remove(path)
                return

        else:
            logging.debug("paths are on the same device")

        bucket_path, data_path = None, None
        try:
            bucket_path, data_path = self._create_storage_bucket()
            metadata_path = bucket_path.joinpath(self.config.get_metadata_filename())
            dst_path = data_path.joinpath(path.name)

            # create metadata and move the data
            self._create_metadata(metadata_path, path)
            path.replace(dst_path)
            logging.debug("%s -> %s", (path.name, dst_path))

        except Exception as e:
            # unwind if possible
            if bucket_path and bucket_path.exists():
                shutil.rmtree(bucket_path)

            logging.error("unable to safely delete path: %s -> %s" % (path, str(e)))
            raise

    def _handle_parent_paths(self, restore_paths, orig_path, entry_uuid):
        if not orig_path.parent.exists():
            if restore_paths:
                logging.debug("will recreate parent path: %s" %
                        (orig_path.parent))
                orig_path.parent.mkdir(
                        mode = self.config.get_directory_mode(),
                        parents = True)
            else:
                logging.error("unable to restore %s as path [%s] doesn't exist" %
                        (entry_uuid, orig_path.parent))
                return False

        return True

    def _restore_path(self, src_path, dst_path):
        if src_path.is_dir():
            shutil.copytree(src_path, dst_path, symlinks = True)
        else:
            shutil.copyfile(src_path, dst_path, follow_symlinks = False)

    def _get_dst_path(self, orig_path, requested_dst_path):
        dst_path = orig_path
        if requested_dst_path:
            dst_path = requested_dst_path.joinpath(orig_path.name)
        return dst_path

    def restore(self, entry_uuid, restore_paths = False, requested_dst_path = None):
        """
        Restore provided entry, identified by its UUID.

        Args:
            entry_uuid (:obj:`uuid.UUID`): UUID of entry to br restored.

            restore_paths(bool, optional): Defaults to False. If True, an
                attempt will be made to recreate the original path (if it doesn't
                exist already) for the file to be restored. If False, and the
                parent path for the file to be restored doesn't exist, the
                operation will fail with an exception.

            requested_dst_path(:obj:`pathlib.Path`, optional): An optional
                path. If provided, the requested file will be restored to the given
                destination rather than to its original path.

        Raises:
            RuntimeError: In case the provided UUID is invalid or doesn't refer
                to existing deleted entry. Also raised if there's any
                filesystem problem with accessing the deleted path's data
                bucket or metadata.

            Exception: In case of any other filesystem problems.

        """
        entry_uuid = util.is_uuid_valid(entry_uuid)
        entry_uuid_str = str(entry_uuid)
        if entry_uuid == None:
            logging.error("invalid uuid provided %s" % (entry_uuid_str))
            raise RuntimeError("Invalid UUID")

        bucket_path = self._get_storage_bucket_path(entry_uuid_str)
        if not bucket_path.exists():
            logging.error("unable to restore %s, unknown identifier" % (entry_uuid_str))
            raise RuntimeError("Unknown UUID")

        metadata = self._load_metadata(bucket_path)
        if not metadata:
            logging.error("unable to obtain metadata for %s" % (entry_uuid_str))
            raise RuntimeError("Metadata unavailable")

        orig_path = Path(metadata['original_path'])
        dst_path = self._get_dst_path(orig_path, requested_dst_path)
        if not self._handle_parent_paths(restore_paths, dst_path, entry_uuid_str):
            raise RuntimeError("Destination parent path problem")

        data_path = bucket_path.joinpath(self.config.get_data_dirname())
        src_path = data_path.joinpath(orig_path.name)
        if not src_path.exists():
            logging.error("no data for %s, unable to restore" % (entry_uuid_str))
            raise RuntimeError("No data in bucket")

        if dst_path.exists():
            logging.error("dest-path already exists %s, won't overwrite" % (orig_path))
            raise RuntimeError("Destination already exists")

        try:
            logging.info("restoring: %s" % (dst_path))
            self._restore_path(src_path, dst_path)

        except Exception as e:
            logging.error("failed to restore: %s -> %s" % (entry_uuid_str, str(e)))
            raise

    def _get_entries_with_metadata(self, path):
        for e in path.iterdir():
            metadata = self._load_metadata(e)
            if not metadata:
                logging.error("bucket: %s has no metadata" % (e))
                continue
            yield(e.name, metadata)

    def list(self):
        """
        List the contents of safe-deleted files that can be restored.

        Returns:
            int: Number of entries safely-deleted - ready to be restored
        """
        print("{:36s} {:8s} {:5s} {:5s} {:26s} {:5s} {}".format(
                "Uuid",
                "Size",
                "Uid",
                "Gid",
                "When deleted",
                "IsDir?",
                "Original Path"))
        print("=" * 105)

        storage_path = self.config.get_storage_path()
        entries = [ (e,m) for e,m in self._get_entries_with_metadata(storage_path) ]
        entries.sort(key = lambda t: t[1]['when_deleted'])

        for entry_uuid, metadata in entries:
            timestamp = datetime.datetime.fromtimestamp(metadata['when_deleted'])
            ts_str = timestamp.isoformat()
            print("{:36s} {:8d} {:5d} {:5d} {:10s} {:5b} {}".format(
                    entry_uuid,
                    metadata['size'],
                    metadata['uid'],
                    metadata['gid'],
                    ts_str,
                    metadata['is_directory'],
                    metadata['original_path']))

        return len(entries)

    def purge(self):
        """
        Removes all safe-deleted files.
        """
        p = self.config.get_storage_path()
        for e in p.iterdir():
            logging.debug("deleting bucket: %s" % (e))
            shutil.rmtree(e)

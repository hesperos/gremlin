import argparse
import logging
import sys

from pathlib import Path

from . import gremlin, gremlin_config, util, wrappers

def rm_action(g, args):
    logging.getLogger().setLevel(
            logging.DEBUG if args.verbose else logging.INFO)
    wrappers.rm_all(g,
            args.interactive,
            args.delete_on_copy,
            args.disable_blacklist,
            *args.files)

def list_action(g, args):
    g.list()

def purge_action(g, args):
    g.purge()

def restore_action(g, args):
    logging.getLogger().setLevel(
            logging.DEBUG if args.verbose else logging.INFO)
    wrappers.restore_all(g,
            args.destination,
            args.recreate,
            args.interactive,
            *args.uuids)

def create_parser():
    ap = argparse.ArgumentParser(prog = Path(sys.argv[0]).name,
            description = "/bin/rm wrapper for safe deleting")
    sp = ap.add_subparsers(dest = 'action',
            required = True,
            help = 'action to perform')

    # =========================================================================
    rmp = sp.add_parser('remove',
            help = 'emulates the default /bin/rm interface for removing files')
    rmp.set_defaults(func = rm_action)

    rmp.add_argument('--delete-on-copy',
            default = False,
            action = 'store_true',
            help = 'If files are on different devices a traditional delete will be done to prevent copying')

    rmp.add_argument('--disable_blacklist',
            default = False,
            action = 'store_true',
            help = "blacklist won't be taken into consideration")

    rmp.add_argument('-r', '--recursive',
            default = False,
            action = 'store_true',
            help = 'Attempt to remove the file hierarchy rooted in each file argument.')

    rmp.add_argument('-f', '--force',
            default = False,
            action = 'store_true',
            help = 'Attempt to remove the files without prompting for confirmation.')

    rmp.add_argument('-i', '--interactive',
            default = False,
            action = 'store_true',
            help = 'Request confirmation before attempting to remove each file.')

    rmp.add_argument('-v', '--verbose',
            default = False,
            action = 'store_true',
            help = 'Be verbose when deleting files, showing them as they are removed.')

    rmp.add_argument('files',
            metavar = 'files',
            type = str,
            nargs = '+',
            help = 'Files to be safely removed')

    # =========================================================================
    listp = sp.add_parser('list',
            help = 'lists stored, safely deleted files, available to be restored')
    listp.set_defaults(func = list_action)

    # =========================================================================
    restorep = sp.add_parser('restore',
            help = 'restores selected, previously deleted files')
    restorep.set_defaults(func = restore_action)

    restorep.add_argument('-v', '--verbose',
            default = False,
            action = 'store_true',
            help = 'Be verbose when restoring files')

    restorep.add_argument('-r', '--recreate',
            default = False,
            action = 'store_true',
            help = "Whether to recreate the destination paths if they don't exist anymore")

    restorep.add_argument('-d', '--destination',
            default = '',
            type = str,
            help = 'Restore file to the provided destination')

    restorep.add_argument('-i', '--interactive',
            default = False,
            action = 'store_true',
            help = 'Request confirmation before attempting to restore each file.')

    restorep.add_argument('uuids',
            metavar = 'uuids',
            type = str,
            nargs = '+',
            help = 'Entry UUIDs to be safely restored')

    # =========================================================================
    purgep = sp.add_parser('purge',
            help = "purges gremlin's datastore")
    purgep.set_defaults(func = purge_action)
    return ap

def main():
    if not util.is_interpreter_supported():
        logging.error("python3 is required")
        sys.exit()

    ap = create_parser()
    gremlin_args, _ = ap.parse_known_args()
    exit_status = 0

    try:
        gc = gremlin_config.GremlinConfig()
        g = gremlin.Gremlin(gc)
        gremlin_args.func(g, gremlin_args)

    except Exception as e:
        logging.error('fatal error: %s' % (str(e)))
        exit_status = 1

    sys.exit(exit_status)

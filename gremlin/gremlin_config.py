import importlib.resources as imres
import logging
import shutil
import yaml
import os

from pathlib import Path
from xdg_base_dirs import xdg_config_home

class GremlinConfig(object):
    """Encapsulates all the configuration for Gremlin class

    Provides an abstract interfaces that is easily mockable and allows
    for testing, grouping together all settings that the Gremlin object
    relies upon.

    Constructor for GremlinConfiguration object requires a base
    directory that will be used to lookup and persist the
    configuration. If it doesn't exist - it will be created.

    Will attempt to copy config templates:
        - blacklist.yaml (contains blacklisted paths and files that
          will be refused to be deleted)
        - config.yaml (contains extra configuration overrides)

    Failures during construction will be logged but are non-critical.
    Configuration will be initialised with the default values
    and won't be persisted.

    Args:
        base_path (str): containing a base configuration path (will be created if doesn't exist).

    Returns:
        GremlinConfig: new instance of GremlinConfig object.
    """

    _DefaultDirectoryMode = 0o744
    _DefaultMetadataFileName = 'METADATA.yaml'
    _DefaultConfigDirname = 'gremlin'
    _DefaultDataDirname = 'data'
    _DefaultStorageDirname = '.gremlin'

    def __init__(self, base_path = xdg_config_home()):
        """Constructs the GremlinConfig object
        """
        self.config_path = Path(base_path).joinpath(GremlinConfig._DefaultConfigDirname)
        self.storage_path = Path.home().joinpath(GremlinConfig._DefaultStorageDirname)
        self.metadata_filename = GremlinConfig._DefaultMetadataFileName
        self.data_dirname = GremlinConfig._DefaultDataDirname

        self.blacklist_paths = []
        self.blacklist_files = []
        self.directory_mode = GremlinConfig._DefaultDirectoryMode

        self._initialise_config_home()
        self._load_config_files()

    def _initialise_config_home(self):
        default_configs = [
                'blacklist.yaml',
                'gremlin.yaml',
                ]

        if not self.config_path.exists():
            self.config_path.mkdir(mode = self.get_directory_mode())

        try:
            for c in default_configs:
                config_file_path = imres.files("gremlin.config").joinpath(c)
                with imres.as_file(config_file_path) as cfp:
                    if not Path(self.config_path).joinpath(c).exists():
                        shutil.copy(cfp, self.config_path)

        except Exception as e:
            logging.error("unable to create default config %s", str(e))

    def _load_blacklist(self):
        blacklist_path = self.config_path.joinpath('blacklist.yaml')
        if not blacklist_path.exists():
            logging.info("blacklist configuration file is missing")
            return

        try:
            data = yaml.safe_load(blacklist_path.read_text())
            self.blacklist_paths = data['paths']
            self.blacklist_files = data['files']

        except Exception as e:
            logging.error(str(e))

    def _load_config(self):
        config_path = self.config_path.joinpath('gremlin.yaml')
        if not config_path.exists():
            logging.info("gremlin configuration file is missing")
            return

        try:
            data = yaml.safe_load(config_path.read_text())
            self.directory_mode = data['DirectoryMode']
        except Exception as e:
            logging.error(str(e))
            # raise RuntimeError("failed to load configuration")

    def _load_config_files(self):
        self._load_blacklist()
        self._load_config()

    def get_directory_mode(self):
        """Returns the default directory mode for data buckets with data
        storage

        Buckets will be created with permissions returned by this
	function. Buckets are directories to which the safe-deleted data
        will be either copied or moved.

        Returns:
            int: access credentials for data buckets.
        """
        return self.directory_mode

    def get_blacklist(self):
        """Returns blacklisted paths and files list

        Returns:
            :rtype: ([], []): The first list contains directories that are forbidden from
            being deleted.  The second list contains files that are forbidden to be deleted.
            The paths are absolute.
        """
        return self.blacklist_paths, self.blacklist_files

    def get_storage_path(self):
        """Returns the storage path
        Storage path is the storage to which your data will be moved or
        copied when safe-deleting with gremlin. Usually it will be
        $HOME/.gremlin

        Returns:
            pathlib.Path: instance defining the storage path
        """
        return self.storage_path

    def get_data_dirname(self):
        """Returns the directory to use as a destination for your data
        when safe-deleting with gremlin.

        The bucket layour is the following:
        <bucket>
        - <metadata_filename>
        - <data_dirname> -> <your data>

        data_dirname may be customized if required. Any change to it
        invalidates already existing data store.

        Returns:
            str: A data directory name within the bucket
        """
        return self.data_dirname

    def get_metadata_filename(self):
        """Returns the filename containing metadata about deleted
        file/directory.

        Metadata encapsulates all the details about deleted file's
        original location, deletion time etc.

        Returns:
            str: A string describing metadata filename.
        """
        return self.metadata_filename

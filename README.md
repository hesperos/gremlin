![tests](https://gitlab.com/hesperos/gremlin/badges/master/pipeline.svg)
![coverage](https://gitlab.com/hesperos/gremlin/badges/master/coverage.svg?job=test)

# gREMlin - `/bin/rm` wrapper for safe file removal

gREMlin is a `rm` replacement that moves your file to a local data store
instead of permanently deleting them so, you can easily restore anything.

## Installation

First install it:

    poetry build
    pip3 install dist/gremlin-0.1.1.tar.gz

In order to use it as a drop-in `/bin/rm` replacement, either create a
wrapper:

```
    cat <<EOF >rm
#!/bin/bash
gremlin remove "$@"
EOF
```

Drop it into your `$HOME/bin` or any other `bin` directory of
higher priority than `/bin` - to eclipse the original `rm`.

Or create an alias:

    alias rm='gremlin remove'

Alias is preferable

From now on all your calls to `rm` will be handled by `gREMlin`.

## Listing deleted files:

	gremlin list

## Restoring files:

	gremlin restore <uuid>

## Cleaning your data store:

	gremlin purge

## Contributions

gREMlin is in a very early stage of development at the moment. Pull
requests, bug reports and other contributions are very welcome.

### Internal API documentation:

Sphinx documentation of gREMlin code is available
[here](https://hesperos.gitlab.io/gremlin).

import mock
import os
import pathlib
import pytest
import sys
import tempfile
import uuid

from gremlin import util

def test_interpreter_support():
    with mock.patch.object(sys, 'version_info') as v_info:
        for v in range(5):
            v_info.major = v
            is_supported = v >= 3
            assert util.is_interpreter_supported() == is_supported

def test_if_detects_paths_on_same_device_correctly():
    with tempfile.NamedTemporaryFile() as t1:
        with tempfile.NamedTemporaryFile() as t2:
            p1 = pathlib.Path(t1.name)
            p2 = pathlib.Path(t2.name)
            assert util.are_paths_on_same_device(p1, p2) == True

def test_if_detects_paths_on_different_devices_correctly():
    mock_p1 = mock.create_autospec(pathlib.Path)
    mock_p2 = mock.create_autospec(pathlib.Path)

    ms1 = mock.create_autospec(os.stat_result)
    ms2 = mock.create_autospec(os.stat_result)

    ms1.st_dev = 1234
    ms2.st_dev = 5678

    mock_p1.stat.return_value = ms1
    mock_p2.stat.return_value = ms2

    assert util.are_paths_on_same_device(mock_p1, mock_p2) == False
    assert mock_p1.stat.called
    assert mock_p2.stat.called

def test_if_uuid_verification_works_correctly():
    testdata = [
            { 'given': 'abcdef', 'expected': False },
            { 'given': 12345, 'expected': False },
            { 'given': None, 'expected': False },
            { 'given': uuid.uuid4(), 'expected': True },
            { 'given': str(uuid.uuid4()), 'expected': True },
            ]

    for td in testdata:
        if td['expected']:
            assert util.is_uuid_valid(td['given'])

        else:
            assert util.is_uuid_valid(td['given']) == None

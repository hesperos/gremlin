import mock
import os
import pytest
import shutil
import tempfile
import uuid

from pathlib import Path

from gremlin import gremlin
from gremlin.gremlin_config import GremlinConfig

def create_test_files(parent_dir, is_dir, mode = 0o755, n_start = 0, n = 8):
    names = []
    for i in range(n_start, n_start + n + 1):
        pattern, func = 'file', Path.touch
        if is_dir:
            pattern, func = 'directory', Path.mkdir

        p = Path(parent_dir).joinpath('%s_%d' % (pattern, i))
        func(p, mode = mode)
        names.append(p)

    return names

def count_files(directory_path):
    return len([ True for f in directory_path.iterdir() ])

def set_default_gremlin_config_mock_expectations(gc_mock, storage_dir_path):
    gc_mock.get_storage_path.return_value = storage_dir_path
    gc_mock.get_blacklist.return_value = [], []
    gc_mock.get_directory_mode.return_value = GremlinConfig._DefaultDirectoryMode
    gc_mock.get_data_dirname.return_value = GremlinConfig._DefaultDataDirname
    gc_mock.get_metadata_filename.return_value = GremlinConfig._DefaultMetadataFileName

def grm(g, path, delete_on_copy, disable_blacklist):
    assert path.exists()
    try:
        g.rm(path, delete_on_copy, disable_blacklist)
    except Exception as e:
        pytest.fail("unexpected exception: %s" % (str(e)))
    assert path.exists() == False

def grestore(g, bucket_id, restore_paths = False, requested_dst_path = None):
    try:
        g.restore(bucket_id, restore_paths, requested_dst_path)
    except Exception as e:
        pytest.fail("unexpected exception: %s" % (str(e)))

def test_if_rm_fails_if_impossible_to_create_storage_bucket():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_parent = Path(storage_dir)
        storage_dir_path = storage_dir_parent.joinpath("ladida")
        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # make storage_dir's parent read only so, it's impossible to
        # neither create storage_dir itself nor the buckets
        storage_dir_parent.chmod(0o500)

        # instantiate object under test
        with pytest.raises(Exception):
            g = gremlin.Gremlin(gc)

        assert storage_dir_path.exists() == False
        storage_dir_parent.chmod(0o755)

def test_if_rm_works_happy_path():
    with tempfile.TemporaryDirectory() as data_dir:
        files, empty_dirs, nonempty_dirs = [], [], []

        # create some files in data_dir
        files = create_test_files(data_dir, False, 0o755, 0, 8)
        empty_dirs = create_test_files(data_dir, True, 0o755, 100, 8)
        nonempty_dirs = create_test_files(data_dir, True, 0o755, 200, 8)
        for d in nonempty_dirs:
            create_test_files(d, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)

            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            file_set = [files, empty_dirs, nonempty_dirs]
            total_files, buckets_total = 0, 0

            for fs in file_set:
                buckets_before = count_files(storage_dir_path)
                file_set_size = len(fs)
                total_files += file_set_size

                for f in fs:
                    grm(g, f, False, False)

                buckets_after = count_files(storage_dir_path)
                new_buckets = buckets_after - buckets_before
                buckets_total += new_buckets
                assert new_buckets == file_set_size

            assert buckets_total == total_files

def test_if_rm_fails_if_no_permissions_to_delete_file():
    with tempfile.TemporaryDirectory() as data_dir:

        parent_rdonly = create_test_files(data_dir, True, 0o755, 0, 1)[0]
        files = create_test_files(parent_rdonly, True, 0o755, 0, 8)

        # make the parent read-only
        parent_rdonly.chmod(0o500)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)

            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            for f in files:
                assert f.exists() == True
                with pytest.raises(Exception):
                    g.rm(f, False, False)
                assert f.exists() == True

def test_if_rm_fails_if_storage_has_incorrect_permissions():
    with tempfile.TemporaryDirectory() as data_dir:
        files = create_test_files(data_dir, True, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_rdonly_path = create_test_files(storage_dir, True, 0o500, 0, 1)[0]

            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_rdonly_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            for f in files:
                assert f.exists() == True
                with pytest.raises(Exception):
                    g.rm(f, False, False)
                assert f.exists() == True

def test_if_rm_incurs_copy_if_different_devices():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)

        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)

        mock_path = mock.create_autospec(Path)

        mock_stat_results = mock.create_autospec(os.stat_result)
        mock_stat_results.st_dev = storage_dir_path.stat().st_dev + 1
        mock_stat_results.st_atime = 0
        mock_stat_results.st_mtime = 0
        mock_stat_results.st_ctime = 0
        mock_stat_results.st_uid = 0
        mock_stat_results.st_gid = 0
        mock_stat_results.st_mode = 0
        mock_stat_results.st_size = 0

        mock_path.stat.return_value = mock_stat_results
        mock_path.is_dir.return_value = False
        mock_path.absolute.return_value = '/some/fake/path'
        mock_path.name = 'some_fake_name'

        try:
            g.rm(mock_path, False, True)
        except Exception as e:
            pytest.fail("unexpected exception: %s" % (str(e)))

        assert mock_path.replace.called


def test_if_rm_deletes_if_different_devices_and_delete_on_copy():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)

        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)

        mock_path = mock.create_autospec(Path)

        mock_stat_results = mock.create_autospec(os.stat_result)
        mock_stat_results.st_dev = storage_dir_path.stat().st_dev + 1
        mock_stat_results.st_atime = 0
        mock_stat_results.st_mtime = 0
        mock_stat_results.st_ctime = 0
        mock_stat_results.st_uid = 0
        mock_stat_results.st_gid = 0
        mock_stat_results.st_mode = 0
        mock_stat_results.st_size = 0

        mock_path.stat.return_value = mock_stat_results
        mock_path.is_dir.return_value = False
        mock_path.absolute.return_value = '/some/fake/path'
        mock_path.name = 'some_fake_name'

        try:
            g.rm(mock_path, True, True)
        except Exception as e:
            pytest.fail("unexpected exception: %s" % (str(e)))

        assert mock_path.unlink.called

        mock_path.is_dir.return_value = True
        with mock.patch('gremlin.gremlin.shutil.rmtree') as mock_rmtree:
            try:
                g.rm(mock_path, True, True)
            except Exception as e:
                pytest.fail("unexpected exception: %s" % (str(e)))
            mock_rmtree.assert_called_with(mock_path)

def test_if_rm_fails_if_path_blacklisted():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)

        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)

        mock_path = mock.create_autospec(Path)

        mock_stat_results = mock.create_autospec(os.stat_result)
        mock_stat_results.st_dev = storage_dir_path.stat().st_dev + 1
        mock_stat_results.st_atime = 0
        mock_stat_results.st_mtime = 0
        mock_stat_results.st_ctime = 0
        mock_stat_results.st_uid = 0
        mock_stat_results.st_gid = 0
        mock_stat_results.st_mode = 0
        mock_stat_results.st_size = 0

        blacklist_path =  '/some/fake/path'
        mock_path.stat.return_value = mock_stat_results
        mock_path.is_dir.return_value = True
        mock_path.absolute.return_value = blacklist_path
        mock_path.name = 'path'
        mock_path.__str__.return_value = blacklist_path

        gc.get_blacklist.return_value = [blacklist_path], []

        with pytest.raises(Exception):
            g.rm(mock_path, False, False)


def test_if_rm_fails_if_file_blacklisted():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)

        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)

        mock_path = mock.create_autospec(Path)

        mock_stat_results = mock.create_autospec(os.stat_result)
        mock_stat_results.st_dev = storage_dir_path.stat().st_dev + 1
        mock_stat_results.st_atime = 0
        mock_stat_results.st_mtime = 0
        mock_stat_results.st_ctime = 0
        mock_stat_results.st_uid = 0
        mock_stat_results.st_gid = 0
        mock_stat_results.st_mode = 0
        mock_stat_results.st_size = 0

        blacklist_path =  '/some/fake/path'
        mock_path.stat.return_value = mock_stat_results
        mock_path.is_dir.return_value = False
        mock_path.absolute.return_value = blacklist_path
        mock_path.name = 'path'
        mock_path.__str__.return_value = blacklist_path

        gc.get_blacklist.return_value = [], [blacklist_path]

        with pytest.raises(Exception):
            g.rm(mock_path, False, False)

def _test_if_restore_works_happy_path():
    with tempfile.TemporaryDirectory() as data_dir:
        files = create_test_files(data_dir, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)
            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            for f in files:
                grm(g, f, False, False)

            for bucket in storage_dir_path.iterdir():
                grestore(bucket.name)

            # check if files are back again in their original location
            for f in files:
                assert f.exists() == True

def test_if_restore_fails_if_bucket_doesnt_exist():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)
        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)

        # generate new UUID and try to restore it with an empty data-store
        bucket_name = str(uuid.uuid4())
        with pytest.raises(Exception):
            g.restore(bucket_name)


def test_if_restore_fails_if_bucket_is_not_accessible():
    with tempfile.TemporaryDirectory() as data_dir:
        files = create_test_files(data_dir, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)
            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            # delete some test files
            for f in files:
                grm(g, f, False, False)

            # iterate over buckets and try to restore files
            for bucket in storage_dir_path.iterdir():
                # remove all permissions - making bucket inaccessible
                bucket.chmod(0)

                with pytest.raises(Exception):
                    g.restore(bucket.name)

                # restore access
                bucket.chmod(0o755)

def test_if_restore_fails_if_bucket_has_no_data():
    with tempfile.TemporaryDirectory() as data_dir:
        files = create_test_files(data_dir, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)
            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            # delete some test files
            for f in files:
                grm(g, f, False, False)

            # iterate over buckets and try to restore files
            for bucket in storage_dir_path.iterdir():
                data_path = bucket.joinpath(gc.get_data_dirname())

                # deliberately remove data
                shutil.rmtree(data_path)

                with pytest.raises(Exception):
                    g.restore(bucket.name)

def test_if_restore_fails_if_bucket_is_missing_metadata():
    with tempfile.TemporaryDirectory() as data_dir:
        files = create_test_files(data_dir, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)
            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            # delete some test files
            for f in files:
                grm(g, f, False, False)

            # iterate over buckets and try to restore files
            for bucket in storage_dir_path.iterdir():
                metadata_path = bucket.joinpath(gc.get_metadata_filename())

                # deliberately remove metadata
                metadata_path.unlink()

                with pytest.raises(Exception):
                    assert g.restore(bucket.name)


def test_if_restore_fails_if_metadata_malformed():
    with tempfile.TemporaryDirectory() as data_dir:
        files = create_test_files(data_dir, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)
            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            # delete some test files
            for f in files:
                grm(g, f, False, False)

            # iterate over buckets and try to restore files
            for bucket in storage_dir_path.iterdir():
                metadata_path = bucket.joinpath(gc.get_metadata_filename())

                # deliberately corrupt metadata
                metadata_path.write_text("some non schema compliant nonsense")

                with pytest.raises(Exception):
                    g.restore(bucket.name)

def test_if_restore_fails_if_uuid_is_invalid():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)
        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)

        invalid_uuids = [ 1234, "hello-there-im-invalid-uuid" ]

        for i in invalid_uuids:
            with pytest.raises(Exception):
                g.restore(i)


def test_if_restore_fails_if_parent_is_not_accessible():
    with tempfile.TemporaryDirectory() as data_dir:
        files = create_test_files(data_dir, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)
            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            # delete some test files
            for f in files:
                grm(g, f, False, False)

            # mark parent as read-only
            Path(data_dir).chmod(0o500)

            # iterate over buckets and try to restore files
            for bucket in storage_dir_path.iterdir():
                with pytest.raises(Exception):
                    g.restore(bucket.name)


def test_if_restore_fails_if_parent_directory_doesnt_exist():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)
        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)

        with tempfile.TemporaryDirectory() as data_dir:
            files = create_test_files(data_dir, False, 0o755, 0, 8)
            # delete some test files
            for f in files:
                grm(g, f, False, False)

        # iterate over buckets and try to restore files
        for bucket in storage_dir_path.iterdir():
            with pytest.raises(Exception):
                g.restore(bucket.name)


def test_if_restore_fails_if_destination_already_exists():
    with tempfile.TemporaryDirectory() as data_dir:
        files = create_test_files(data_dir, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)
            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            # delete some test files
            for f in files:
                grm(g, f, False, False)

            # iterate over buckets and try to restore files
            for bucket in storage_dir_path.iterdir():
                # first call should succeed
                grestore(g, bucket.name)

                # second call should fail
                with pytest.raises(Exception):
                    g.restore(bucket.name)


def test_if_restore_recreates_paths_if_requested():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)
        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)
        data_dir_path = None

        with tempfile.TemporaryDirectory() as data_dir:
            data_dir_path = Path(data_dir)
            files = create_test_files(data_dir, False, 0o755, 0, 8)
            # delete some test files
            for f in files:
                grm(g, f, False, False)

        # iterate over buckets and try to restore files
        for bucket in storage_dir_path.iterdir():
            grestore(g, bucket.name, True)

        if data_dir_path.exists():
            shutil.rmtree(data_dir_path)

def test_if_restore_restores_to_provided_path():
    with tempfile.TemporaryDirectory() as storage_dir:
        storage_dir_path = Path(storage_dir)
        gc = mock.create_autospec(GremlinConfig)
        set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

        # instantiate object under test
        g = gremlin.Gremlin(gc)
        data_dir_path = None

        with tempfile.TemporaryDirectory() as data_dir:
            data_dir_path = Path(data_dir)
            files = create_test_files(data_dir, False, 0o755, 0, 8)
            # delete some test files
            for f in files:
                grm(g, f, False, False)

        # iterate over buckets and try to restore files
        with tempfile.TemporaryDirectory() as data_dir2:
            data_dir2_path = Path(data_dir2)
            for bucket in storage_dir_path.iterdir():
                grestore(g, bucket.name, True, data_dir2_path)

            assert count_files(data_dir2_path) > 0

def test_if_list_ignores_buckets_with_malformed_metadata():
    with tempfile.TemporaryDirectory() as data_dir:
        files, empty_dirs, nonempty_dirs = [], [], []

        # create some files in data_dir
        files = create_test_files(data_dir, False, 0o755, 0, 8)
        empty_dirs = create_test_files(data_dir, True, 0o755, 100, 8)
        nonempty_dirs = create_test_files(data_dir, True, 0o755, 200, 8)
        for d in nonempty_dirs:
            create_test_files(d, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)

            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            file_set = [files, empty_dirs, nonempty_dirs]

            for fs in file_set:
                for f in fs:
                    grm(g, f, False, False)

            # iterate over buckets and corrupt metadata for half of them
            cnt = 0
            n_corrupted = 0
            for bucket in storage_dir_path.iterdir():
                metadata_path = bucket.joinpath(gc.get_metadata_filename())

                if (cnt % 2) == 0:
                    # deliberately corrupt metadata
                    metadata_path.write_text("some non schema compliant nonsense")
                    n_corrupted += 1

                cnt += 1

            num_entries = g.list()
            assert num_entries == (cnt - n_corrupted)

def test_if_purge_removes_all_data():
    with tempfile.TemporaryDirectory() as data_dir:
        files, empty_dirs, nonempty_dirs = [], [], []

        # create some files in data_dir
        files = create_test_files(data_dir, False, 0o755, 0, 8)
        empty_dirs = create_test_files(data_dir, True, 0o755, 100, 8)
        nonempty_dirs = create_test_files(data_dir, True, 0o755, 200, 8)
        for d in nonempty_dirs:
            create_test_files(d, False, 0o755, 0, 8)

        with tempfile.TemporaryDirectory() as storage_dir:
            storage_dir_path = Path(storage_dir)

            gc = mock.create_autospec(GremlinConfig)
            set_default_gremlin_config_mock_expectations(gc, storage_dir_path)

            # instantiate object under test
            g = gremlin.Gremlin(gc)

            file_set = [files, empty_dirs, nonempty_dirs]

            for fs in file_set:
                for f in fs:
                    grm(g, f, False, False)

            assert count_files(storage_dir_path) > 0
            try:
                g.purge()
            except Exception as e:
                pytest.fail("unexpected exception: %s" % (str(e)))
            assert count_files(storage_dir_path) == 0

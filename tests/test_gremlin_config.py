import os
import pytest
import tempfile

import pathlib

from unittest.mock import patch
from gremlin import gremlin_config as cfg

def test_if_gc_initialises_home_direcory_config_on_construction():
    with tempfile.TemporaryDirectory() as tmp_dir:
        c = cfg.GremlinConfig(tmp_dir)
        assert c.config_path.exists() == True
        assert len([ f for f in c.config_path.iterdir() ]) > 0

def test_if_gc_doesnt_overwrite_home_configs():
    with tempfile.TemporaryDirectory() as tmp_dir:
        c1 = cfg.GremlinConfig(tmp_dir)
        mtimes1 = { f: f.stat() for f in c1.config_path.iterdir() }

        c2 = cfg.GremlinConfig(tmp_dir)
        mtimes2 = { f: f.stat() for f in c2.config_path.iterdir() }

        assert mtimes2 == mtimes1


def test_if_doesnt_fail_if_unable_to_find_config_templates():
    with tempfile.TemporaryDirectory() as tmp_dir:
        with patch('importlib.resources.as_file') as mock_as_file:
            # mock the context manager return value
            mock_as_file.return_value.__enter__.return_value = None
            c = cfg.GremlinConfig(tmp_dir)
            assert len([ f for f in c.config_path.iterdir() ]) == 0

def test_if_doesnt_fail_if_blacklists_are_malformed():
    with tempfile.TemporaryDirectory() as tmp_dir:
        with tempfile.TemporaryDirectory() as templ_dir:
            cfg_path = os.path.join(templ_dir, 'config')
            os.mkdir(cfg_path)
            blacklist_path = os.path.join(cfg_path, "blacklist.yaml")

            # create malformed configuration templates
            with open(blacklist_path, "w") as blacklist_fh:
                blacklist_fh.write("nonsense df sdfds---")

            with patch('importlib.resources.as_file') as mock_as_file:
                mock_as_file.return_value.__enter__.return_value = blacklist_path
                c = cfg.GremlinConfig(tmp_dir)

def test_if_doesnt_fail_if_configs_are_malformed():
    with tempfile.TemporaryDirectory() as tmp_dir:
        with tempfile.TemporaryDirectory() as templ_dir:
            cfg_path = os.path.join(templ_dir, 'config')
            os.mkdir(cfg_path)
            gremlin_cfg_path = os.path.join(cfg_path, "gremlin.yaml")

            # create malformed configuration templates
            with open(gremlin_cfg_path, "w") as blacklist_fh:
                blacklist_fh.write("nonsense df sdfds---")

            with patch('importlib.resources.as_file') as mock_as_file:
                mock_as_file.return_value.__enter__.return_value = gremlin_cfg_path
                c = cfg.GremlinConfig(tmp_dir)

def test_if_returns_nonempty_paths():
    with tempfile.TemporaryDirectory() as tmp_dir:
        c = cfg.GremlinConfig(tmp_dir)
        assert len(str(c.get_metadata_filename())) > 0
        assert len(str(c.get_data_dirname())) > 0
        assert len(str(c.get_storage_path())) > 0

def test_if_returns_valid_values_for_non_string_methods():
    with tempfile.TemporaryDirectory() as tmp_dir:
        c = cfg.GremlinConfig(tmp_dir)

        assert c.get_directory_mode() != 0

        blacklist_paths, blacklist_files = c.get_blacklist()

        assert isinstance(blacklist_paths, list)
        assert isinstance(blacklist_files, list)

FROM python:3.11-alpine

RUN apk add make

RUN adduser -S -u 1234 -h /home/gremlin_tester gremlin_tester gremlin_tester
USER gremlin_tester
ENV PATH="/home/gremlin_tester/.local/bin:$PATH"

RUN pip3 install --user sphinx sphinx-rtd-theme pipx
RUN pipx install poetry==1.4.2
